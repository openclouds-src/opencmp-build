#!/bin/bash

set -ex

basedir="$(realpath $(dirname $0))"
builddir="${basedir}/build.host"
resultdir="${builddir}/result"

build_image() {
    echo "Building host image ..."
    rm -rf ${builddir}/*
    cp -av ${basedir}/boot.iso ${builddir}/boot.iso
    mkdir -p ${builddir}/tmp
    livemedia-creator \
        --ram 8192 \
        --vcpus 16 \
        --vnc vnc=0.0.0.0:10 \
        --iso ${builddir}/boot.iso \
        --make-disk \
        --image-name host.img \
        --ks build-host.ks \
        --resultdir ${resultdir} \
        --tmp ${builddir}/tmp \
        --logfile ${builddir}/build_host.log
    cp -v ${resultdir}/{host.img,host.img.bak}
}

build_product_img() {
    # build product.img
    local tempdir=$(mktemp -d)
    cp -av ${basedir}/data/product/* ${tempdir}/
    cat > "${tempdir}/.buildstamp" <<EOF
[Main]
Product=${HOST}
Version=V${version}
BugURL=https://bugzilla.redhat.com
IsFinal=True
UUID=${timestamp:0:8}.x86_64
[Compose]
Lorax=21.30-1
EOF
    pushd ${tempdir}
        find . | cpio -c -o --quiet | pigz -9c > ${builddir}/product.img
    popd
    rm -rf ${tempdir}
}

main() {
    mkdir -p ${builddir}
    build_image
    #build_product_img
}

main

