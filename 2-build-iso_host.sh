#!/bin/bash

set -ex

workdir=$(dirname $(realpath $0))
resultdir=${workdir}/build.host/result
product=cManager
version=2.0
timestamp=$(date +%Y%m%d%H%M)

# build squashfs image
build_squashfs() {
    local image=${resultdir}/host.img
    local mntdir=$(mktemp -d)
    local loopdev=$(losetup -f)
    losetup ${loopdev} ${image}
    kpartx -avs ${loopdev}
    mount /dev/mapper/$(basename ${loopdev})p1 ${mntdir}/

    local tmpdir=$(mktemp -d)
    local mntdir_root=$(mktemp -d)
    mkdir -p ${tmpdir}/squashfs_root/LiveOS
    truncate -s 20G ${tmpdir}/squashfs_root/LiveOS/rootfs.img
    loopdev_root=$(losetup -f)
    losetup ${loopdev_root} ${tmpdir}/squashfs_root/LiveOS/rootfs.img
    mkfs.ext4 -L LiveOS -b 1024 -m 0 ${loopdev_root}
    mount ${loopdev_root} ${mntdir_root}
    
    cp -a ${mntdir}/. ${mntdir_root}/
    
    mount -t proc proc ${mntdir_root}/proc
    mount -t sysfs sys ${mntdir_root}/sys
    mount -t devtmpfs dev ${mntdir_root}/dev
    
    chroot ${mntdir_root} setfiles -e /proc -e /sys -e /dev /etc/selinux/targeted/contexts/files/file_contexts /
    
    umount ${mntdir}
    umount -R ${mntdir_root}
    rm -rf ${mntdir} ${mntdir_root}
    
    kpartx -dvs ${loopdev}
    losetup -d ${loopdev}
    losetup -d ${loopdev_root}
    
    mksquashfs ${tmpdir}/squashfs_root ${resultdir}/live-rootfs.squashfs.img -noappend
    
    rm -rf ${tmpdir}
}

# build iso
build_iso() {
    local mntdir=$(mktemp -d)
    local tmpdir=$(mktemp -d)
    local efidir=$(mktemp -d)

    mount ${workdir}/boot.iso ${mntdir}
    cp -a ${mntdir}/. ${tmpdir}/
    umount ${mntdir}/
    rm -rf ${mntdir}/

    cp ${resultdir}/live-rootfs.squashfs.img ${tmpdir}/
    cp ${workdir}/install-host.ks ${tmpdir}/

    mount ${tmpdir}/images/efiboot.img ${efidir}
    local cfgs="${tmpdir}/EFI/BOOT/grub.cfg ${tmpdir}/isolinux/isolinux.cfg ${tmpdir}/isolinux/grub.conf ${efidir}/EFI/BOOT/grub.cfg"
    local orig_name=$(grep -Po "(?<=^menu title ).*" ${tmpdir}/isolinux/isolinux.cfg)
    sed -i \
      -e "s/hd:LABEL=[^ :]*/hd:LABEL=${product}/g" \
      -e "/stage2/ s%$% inst.ks=hd:LABEL=${product}:/install-host.ks%" \
      -e "/^\s*\(append\|initrd\|linux\|search\)/! s%${orig_name}%${product}%g" \
      -e "s/Rescue a .* system/Rescue a ${product_name} system/g" \
      ${cfgs}
    sed -i "/search/s/'.*'/'${product}'/" ${tmpdir}/EFI/BOOT/grub.cfg ${efidir}/EFI/BOOT/grub.cfg
    umount ${efidir}
    rm -rf ${efidir}

    rm -fv ${product}-*.iso

    mkisofs -J -T -U \
        -joliet-long \
        -allow-limited-size \
        -o ${product}.iso \
        -b isolinux/isolinux.bin \
        -c isolinux/boot.cat \
        -no-emul-boot \
        -boot-load-size 4 \
        -boot-info-table \
        -eltorito-alt-boot \
        -e images/efiboot.img \
        -no-emul-boot \
        -R \
        -graft-points \
        -A "${product}" \
        -V "${product}" \
        -publisher "massclouds.com" \
        ${tmpdir}
    isohybrid -u ${product}.iso
    implantisomd5 --force ${product}.iso

    mv -v ${product}.iso ${product}-V${version}-${timestamp}.iso
    rm -rf ${tmpdir}
}

build_squashfs
build_iso

