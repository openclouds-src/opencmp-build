
url --url=http://192.168.106.1/repos/baseos
repo --name=appstream --baseurl=http://192.168.106.1/repos/appstream
repo --name=powertools --baseurl=http://192.168.106.1/repos/powertools
repo --name=extras --baseurl=http://192.168.106.1/repos/extras

lang en_US.UTF-8
keyboard us
timezone --utc Asia/ShangHai
network --noipv6
auth --enableshadow --passalgo=sha512
selinux --permissive
firewall --disabled

rootpw --plaintext 1QAZ@wsx3edc
firstboot --reconfig
clearpart --all --initlabel
bootloader --timeout=1
part / --size=102400 --fstype=xfs --fsoptions=discard
poweroff

%packages --excludedocs --ignoremissing --excludeWeakdeps
dracut-config-generic
-dracut-config-rescue
dracut-live
python36
centos-stream-repos
scap-security-guide

# vim
vim

# lvm
lvm2

# podman
podman
podman-plugins

%end

%post --erroronfail
set -x
mkdir -p /etc/yum.repos.d
rm -f /etc/yum.repos.d/*.repo

systemctl enable sshd

rm -f /etc/sysconfig/network-scripts/*

%end

