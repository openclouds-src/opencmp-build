#!/bin/bash

set -e

workdir=$(dirname $(realpath $0))
resultdir=${workdir}/build.host/result

vm_name="opencmp_node"

echo "destroy and undefine VM opencmp_node ..."
virsh destroy ${vm_name} &>/dev/null || :
virsh undefine ${vm_name} &>/dev/null || :

echo "destroy and undefine default network ..."
virsh net-destroy default &>/dev/null || :
virsh net-undefine default &>/dev/null || :

echo "define and start default network ..."
virsh net-define /usr/share/libvirt/networks/default.xml
virsh net-start default

mac_addr=$( printf '52:54:00:%02x:%02x:%02x\n' $[RANDOM%177] $[RANDOM%255] $[RANDOM%255])

echo "VM mac_addr: ${mac_addr}"

cp -fv ${resultdir}/{host.img.bak,host.img}

echo "Running vm ..."
virt-install \
    -n ${vm_name} \
    --os-variant rhel8.0 \
    --virt-type kvm \
    --memory 65536 \
    --vcpus 8 \
    --network network=default,mac=${mac_addr},model=virtio \
    --disk ${resultdir}/host.img \
    --import \
    --noautoconsole \
    --rng /dev/random \
    --graphics vnc,port=5910,listen=0.0.0.0 \
    --video vga \
    --sound none \
    --controller usb,model=none \
    --memballoon none \
    --boot hd,menu=off \
    --clock kvmclock_present=yes

echo "Get vm IP ..."
vm_ip=
i=0
while [[ $i -lt 60 ]]; do
    sleep 2
    vm_ip=$(virsh net-dhcp-leases default | grep -i ${mac_addr} | awk '{print $5}' | cut -d '/' -f 1)
    if [ -n "${vm_ip}" ]; then
        echo "VM ipaddress: ${vm_ip}"
        break
    fi
    i=$((i+1))
done

if [ -z "${vm_ip}" ]; then
    echo "Failed to get vm ip !"
    exit 1
fi

ssh_opts="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=ERROR -o ConnectTimeout=3"
ssh_passwd="1QAZ@wsx3edc"
echo "Wait for ssh to vm ..."
i=0
while [[ $i -lt 60 ]]; do
    sleep 1
    if sshpass -p ${ssh_passwd} ssh ${ssh_opts} root@${vm_ip} exit 0; then
        echo "SSH to vm OK !"
        break
    fi
    i=$((i+1))
done

if [[ $i -eq 60 ]]; then
    echo "Ssh to vm timeout !"
    exit 1
fi

echo "copying opencmp-deploy.tar.gz to vm ..."
sshpass -p ${ssh_passwd} scp ${ssh_opts} ${workdir}/opencmp-deploy.tar.gz root@${vm_ip}:/tmp

echo "execing deploy.sh ..."
sshpass -p ${ssh_passwd} ssh ${ssh_opts} root@${vm_ip} \
    'set -e; \
     tar -xf /tmp/opencmp-deploy.tar.gz -C /tmp; \
     cd /tmp/opencmp-deploy; bash deploy.sh; \
     cd -; rm -rf /tmp/opencmp-deploy*'

virsh destroy ${vm_name} &>/dev/null

