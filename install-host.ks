lang zh_CN.UTF-8
keyboard --vckeymap=us --xlayouts='us'
timezone --utc Asia/Shanghai
auth --enableshadow --passalgo=sha512
selinux --permissive
firewall --disabled

rootpw --plaintext 1QAZ@wsx3edc

autopart --nohome

liveimg --url=file:///run/install/repo/live-rootfs.squashfs.img

poweroff

%post --erroronfail

# change language
sed -i 's/zh_CN/en_US/' /etc/locale.conf
## end 

%end

